#!/usr/bin/python3
# coding: utf8
# © 2017-2019 Cyril Brulebois <kibi@debian.org>

import os
import subprocess
import time

import PIL.Image
import PIL.ImageChops
import PIL.ImageStat
from kadit.utils import find_image_file

from colorama import Fore


VALID_SIZES = [(640, 480), (720, 400), (800, 600), (1024, 768), (1280, 800)]


class KaditReferenceImage:
    def __init__(self, name, search_paths, auto_mask=True):
        # FIXME: Handle errors
        self.filename, self.mask_stuff = find_image_file(name, search_paths)
        self.image = PIL.Image.open(self.filename)
        if auto_mask:
            self.image = auto_apply_mask(self.image)
        self.image = self.get_masked_image(self.image)

    def get_masked_image(self, image, debug=False):
        if len(self.mask_stuff) == 0:
            if debug:
                print("no mask to apply")
            return image
        elif image.size != self.image.size:
            if debug:
                print("not applying mask, size mismatch")
            return image
        elif image.size != self.image.size:
            return image
        else:
            if debug:
                print("applying mask")
            mask_path, white_path = self.mask_stuff
            white = PIL.Image.open(white_path)
            mask = PIL.Image.open(mask_path)
            return PIL.Image.composite(image, white, mask)


def get_screenshot(winid, snapdir, basename=None, retry=None):
    while 1:
        if basename is None:
            basename = '%f' % time.time()
        screenshot_path = snapdir / ('%s.png' % basename)
        ret = subprocess.call(['import',
                               '-window', '%d' % winid,
                               screenshot_path],
                              stdout=subprocess.DEVNULL,
                              stderr=subprocess.DEVNULL)
        if ret == 0:
            return screenshot_path
        elif retry is not None:
            print(Fore.RED + 'SCR. FAIL ', end=' ')
            print('unable to save %s (likely: missing dir or window not visible)'
                  % screenshot_path)
            time.sleep(retry)
        else:
            raise RuntimeError('Unable to generate screenshot')


def is_same_image(im1, im2):
    if im1.mode != im2.mode:
        # print("Image mode mismatch: %s vs. %s" % (im1.mode, im2.mode))
        return False
    elif im1.size != im2.size:
        # print("Size mismatch: %s vs. %s" % (im1.size, im2.size))
        return False
    else:
        return PIL.ImageChops.difference(im1, im2).getbbox() is None


def compute_diff_size(im1, im2):
    # No need to go further:
    if im1.mode != im2.mode:
        return None
    elif im1.size != im2.size:
        return None

    diff = PIL.ImageChops.difference(im1, im2)
    bbox = diff.getbbox()
    if bbox is None:
        return 0
    else:
        (x1, y1, x2, y2) = bbox # pylint: disable=invalid-name
        return (x2-x1) * (y2-y1)


def compute_diff_rms(im1, im2):
    # No need to go further:
    if im1.mode != im2.mode:
        return None
    elif im1.size != im2.size:
        return None

    diff = PIL.ImageChops.difference(im1, im2)
    stat = PIL.ImageStat.Stat(diff)

    # beware, only a single item if dealing with greyscale images:
    if len(stat.rms) == 1:
        return stat.rms * 3
    else:
        return stat.rms


def has_valid_screenshot_size(image):
    if (image.size) not in VALID_SIZES:
        print('Unknown size:', image.size)
        print('Hint: move the window to make sure no part is hidden')
        return False
    else:
        return True


def get_top_margin(image):
    for size in VALID_SIZES:
        if image.size[0] == size[0]:
            top_margin = image.size[1] - size[1]
            assert top_margin >= 0
            print('detected top margin: %d' % top_margin)
            return top_margin
    raise RuntimeError('unable to detect top margin for size=%dx%d' % image.size)


# Automatically apply mask:
def auto_apply_mask(image):
    if image.size == (800, 600):
        white_file = 'playbooks/masks/800x600-white.png'
        mask_file = 'playbooks/masks/800x600-banner.png'
        white = PIL.Image.open(white_file)
        mask = PIL.Image.open(mask_file)
        return PIL.Image.composite(image, white, mask)
    else:
        return image


# Most useful for libvirt screenshot with menu bar:
def crop_image(image, top_margin):
    width, height = image.size
    return image.crop((0, top_margin, width, height))
