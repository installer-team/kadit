#!/usr/bin/python3
# coding: utf8
# © 2017-2019 Cyril Brulebois <kibi@debian.org>

from __future__ import print_function

import os.path
import pprint
import re
import sys

from operator import xor

import kadit.imaging as ki
import kadit.utils as ku
import yaml
from colorama import Fore


def load_from_playbook(playbook_file, local_debug=True):

    # Local logger-like function:
    def eprint(*args, **kwargs):
        if local_debug:
            print(*args, file=sys.stderr, **kwargs)
        else:
            pass

    with open(playbook_file, 'r') as yamlfile:
        cfg = yaml.safe_load(yamlfile)

    # Search paths for images:
    image_search_paths = [ku.get_playbook_basedir(playbook_file)]

    # Only to be used during initialization:
    add_steps = []
    remove_steps = []
    change_steps = []

    steps = []
    settings = {}
    for item in cfg:
        item_type = list(item.keys())[0]
        if item_type == 'step':
            eprint(Fore.YELLOW + 'ADD STEP  ', end=' ')
            eprint(item['step']['name'])
            steps.append(item['step'])
        elif item_type == 'settings':
            for k in item['settings'].keys():
                eprint(Fore.GREEN + 'SETTINGS  ', end=' ')
                eprint(k)
                settings[k] = item['settings'][k]
                # Process special settings: inheritance, steps to be
                # added, changed, or removed.
                if k == 'inherit':
                    inherit_file = os.path.join(os.path.dirname(playbook_file),
                                                item['settings'][k])
                    with open(inherit_file, 'r') as inherit_yamlfile:
                        inherit_cfg = yaml.safe_load(inherit_yamlfile)
                    cfg.extend(inherit_cfg)
                    additional_search_path = ku.get_playbook_basedir(inherit_file)
                    image_search_paths.append(additional_search_path)
                elif k == 'add_step' or k == 'add_steps':
                    add_steps.extend(item['settings'][k])
                elif k == 'remove_step' or k == 'remove_steps':
                    remove_steps.extend(item['settings'][k])
                elif k == 'change_step' or k == 'change_steps':
                    change_steps.extend(item['settings'][k])
                elif k == 'image_matching':
                    pass
                elif k == 'alternatives':
                    pass
                elif k == 'setup':
                    pass
                else:
                    eprint(Fore.RED + 'UNKNOWN   ', end=' ')
                    eprint("Unknown setting:", k)
                    sys.exit(1)
        else:
            eprint(Fore.RED + 'UNKNOWN   ', end=' ')
            eprint("Unknown type:", item_type)
            sys.exit(1)

    get_names = lambda step_list: [list(x.keys())[0] for x in step_list]

    # Post-process steps, start with removals:
    steps = [x for x in steps if x['name'] not in remove_steps]

    # Post-process steps, continue with additions:
    for add_id, add_name in enumerate(get_names(add_steps)):
        eprint(Fore.RED + 'ADD STEP  ', end=' ')
        eprint(add_name)

        # Check properties:
        item = add_steps[add_id][add_name]
        after = item.get('after', None)
        before = item.get('before', None)
        assert xor(after is not None, before is not None)

        # Look for the reference step:
        ref_name = after or before
        ref_id = [x['name'] for x in steps].index(ref_name)

        # Adjust position if needed:
        if after:
            ref_id += 1

        # Add name properties, remove after/before, and insert:
        item['name'] = add_name
        item.pop('after', None)
        item.pop('before', None)
        steps.insert(ref_id, item)

    # Post-process steps, finish with changes:
    #
    # Note: This is ugly, but ported from code that was previously
    # working...
    for change_id, change_name in enumerate(get_names(change_steps)):
        if change_name in [x['name'] for x in steps]:
            eprint(Fore.RED + 'CHG STEP  ', end=' ')
            eprint(change_name)

            ref_id = [x['name'] for x in steps].index(change_name)

            # Override keys: should be able to remove some by settings
            # them to [], None, etc. as needed:
            for k in change_steps[change_id][change_name].keys():
                steps[ref_id][k] = change_steps[change_id][change_name][k]
        else:
            print("No step to change matching name: %s" % change_name)
            sys.exit(1)

    return settings, steps, image_search_paths


def get_reference_images_for(step, image_search_paths, settings):
    """Return the images that are acceptable for a given step, be them
    specified as a single image or as a list of images"""
    image = step['wait']
    if isinstance(image, list):
        images = image
    else:
        images = [image]

    auto_mask = step.get('auto_mask', True)
    ref_images = []
    for image in images:
        ref_image = ki.KaditReferenceImage(image, image_search_paths, auto_mask)
        print(Fore.MAGENTA + 'NEEDED    ', end=' ')
        print('%s => %s' % (image, ref_image.filename))
        ref_images.append(ref_image)

        # Try and load different images (different suite, different rendering, etc.):
        for suffix in settings.get('alternatives', []):
            try:
                possible_extra_image = re.sub(r'\.png$', '%s.png' % suffix, image)
                extra_ref_image = ki.KaditReferenceImage(possible_extra_image, image_search_paths, auto_mask)
                print(Fore.MAGENTA + 'NEEDED [+]', end=' ')
                print('%s => %s' % (image, extra_ref_image.filename))
                ref_images.append(extra_ref_image)
            except:
                pass

    return ref_images


if __name__ == '__main__':
    assert len(sys.argv) == 2
    PLAYBOOK_FILE = sys.argv[1]
    _, STEPS, _ = load_from_playbook(PLAYBOOK_FILE, False)
    pprint.pprint(STEPS)
