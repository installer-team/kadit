#!/usr/bin/python3
# coding: utf8
# © 2017-2019 Cyril Brulebois <kibi@debian.org>

import re
import subprocess
import sys
import time
from pathlib import Path

import PIL.Image
import kadit.imaging as ki
import xdo

from colorama import Fore


def get_current_desktop():
    output = subprocess.check_output(['wmctrl', '-d']).decode()
    for line in output.rstrip().split('\n'):
        elements = line.split()
        if elements[1] == '*':
            return elements[0]
    return -1


def get_windows(desktop_id, always_visible=True):
    output = subprocess.check_output(['wmctrl', '-l']).decode()
    windows = []
    for line in output.rstrip().split('\n'):
        elements = line.split()
        if elements[1] == desktop_id or (always_visible and elements[1] == '-1'):
            title = line[20:]
            windows.append([elements[0], title])
    return windows


def get_xdo_robot(debug=False):
    robot = xdo.xdo()

    if debug:
        version = xdo.version()
        print("libxdo version:", version)

    if getattr(robot, 'enter_text_window', None) is None:
        print("Your python-xdo package seems too old")
        print("It's lacking get_enter_text_window, added in 0.4")
        sys.exit(1)

    return robot


def reset_window_to_center(wid, x, y, debug=False):
    # Get current location (hopefully --shell has a stable output,
    # which might not be the case for the non-shell one):
    current = {}
    current_cmd = ['xdotool', 'getmouselocation', '--shell']
    current_output = subprocess.check_output(current_cmd).decode()

    # "Parse" and store every variable:
    variable = re.compile(r'([^=]+)=(.*)')
    for line in current_output.rstrip().split('\n'):
        res = variable.match(line)
        if res:
            if debug:
                print('found match:', res.group(1), '=>', res.group(2))
            key, value = res.group(1), res.group(2)
            current[key] = value

    # Make sure we've got the bare minimum:
    if 'X' not in current or 'Y' not in current:
        print('Missing X or Y in output:', current_output)
        sys.exit(1)

    # Move pointer relatively to the specified window (if absolute is
    # preferred, xwininfo should be helpful):
    move_cmd = ['xdotool', 'mousemove', '--window', '%d' % wid,
                '%d' % x, '%d' % y]
    subprocess.check_call(move_cmd)

    # NOTE: this can't work, moving back means triggers a move inside the VM.
    # # Move pointer back to previous position:
    # move_back_cmd = ['xdotool', 'mousemove', current['X'], current['Y']]
    # subprocess.check_call(move_back_cmd)


def get_window_size(wid):
    # Get window information:
    xwininfo_cmd = ['xwininfo', '-id', '%d' % wid]
    xwininfo_output = subprocess.check_output(xwininfo_cmd).decode()

    # "Parse" and store interesting variables:
    props = {}
    variable = re.compile(r'\s*(Width|Height): (\d+)')
    for line in xwininfo_output.rstrip().split('\n'):
        res = variable.match(line)
        if res:
            print('found something interesting in:', line)
            props[res.group(1)] = res.group(2)

    # Make sure we've got the bare minimum:
    if 'Width' not in props or 'Height' not in props:
        print('Missing Width or Height in:', xwininfo_output)
        sys.exit(1)

    return int(props['Width']), int(props['Height'])


class KaditAutomat:
    # Azerty vs. qwerty kludge:
    TO_QWERTY = str.maketrans('azqwAZQW;:mM/.-',
                              'qwazQWAZmM,?!:)')

    def __init__(self, workdir):
        print('Initializing KaditAutomat')
        self.snapdir = Path(workdir) / 'snaps'
        self.snapdir.mkdir(parents=True, exist_ok=True)
        self.top_margin = 0
        self.autodetect_virtualization()
        self.robot = get_xdo_robot()
        self.focus = False

    def autodetect_virtualization(self):
        # Check current desktop and its windows:
        all_wins = get_windows(get_current_desktop())

        # Checking QEMU windows:
        candidates = [x for x, y in all_wins if y.startswith('QEMU')]
        if len(candidates) != 0:
            if len(candidates) > 1:
                raise RuntimeError('Several QEMU windows detected')
            self.winid = int(candidates[0], 0)
            self.virt = 'qemu'
            return

        # Checking libvirt windows:
        for libvirt in ['QEMU', 'QEMU/KVM', 'Virt Viewer', 'Virtual Machine']:
            candidates = [x for x, y in all_wins if y.endswith(libvirt) > 0]
            if len(candidates) != 0:
                if len(candidates) > 1:
                    raise RuntimeError('Several libvirt windows detected')
                self.winid = int(candidates[0], 0)
                self.virt = 'libvirt'

                # Determine top margin (menu bar):
                screenshot_file = ki.get_screenshot(self.winid, self.snapdir)
                image = PIL.Image.open(screenshot_file)
                self.top_margin = ki.get_top_margin(image)
                return

        raise RuntimeError('Found no QEMU or libvirt windows')

    def try_menu_tweak(self):
        assert self.virt == 'qemu'
        self.send_key('ctrl+alt+m')
        time.sleep(0.5)

    def reset_mouse_to_center(self):
        w, h = get_window_size(self.winid)
        reset_window_to_center(self.winid, w/2, h/2+self.top_margin)

    def send_key(self, key):
        """Use either XSendEvent or XTEST depending no virt type

        With XFCE in 2017:
         - qemu: XSendEvent (with a target window)
         - libvirt: XTEST (without a target window, raising it)

        With GNOME (X.Org) on Bullseye in 2023:
         - qemu: activate window (stronger than raising it), then don't target a
           specific window (xdotool key --window doesn't seem to work either)...
           except keys shouldn't go to unrelated windows, so keep the window=
           parameter to avoid friendly fire (making that a necessary condition
           even if it's not a sufficient one).
         - libvirt: untested.
        """
        if self.virt == 'qemu':
            if not self.focus:
                subprocess.call(['xdotool', 'windowactivate', '%d' % self.winid])
                time.sleep(0.5)
                self.focus = True
            # FIXME: Why the off-by-one?! Seems to happen with both the Python
            # bindings and with the xdotool CLI.
            self.robot.send_keysequence_window(key, window=self.winid+1)
        elif self.virt == 'libvirt':
            # FIXME: This is ugly:
            subprocess.call(['xdotool', 'windowraise', '%d' % self.winid])
            self.robot.send_keysequence_window(key)
        else:
            raise NotImplementedError('no send_key for virt=%s' % self.virt)


    def send_keys_for_step(self, step):
        keys = step['keys']
        print(Fore.CYAN + "TYPING    ", end=' ')

        final_keys = []
        for key in keys:
            if key.startswith('str:'):
                candidate = key[4:]
                if step.get('layout', 'azerty') == 'qwerty':
                    candidate = candidate.translate(self.TO_QWERTY)
                final_keys.extend(list(candidate))
            else:
                final_keys.append(key)

        # rewrite a few things:
        for i, _ in enumerate(final_keys):
            if final_keys[i] == '/':
                final_keys[i] = 'slash'
            elif final_keys[i] == '.':
                final_keys[i] = 'period'
            elif final_keys[i] == ',':
                final_keys[i] = 'comma'
            elif final_keys[i] == '=':
                final_keys[i] = 'equal'
            elif final_keys[i] == '!':
                final_keys[i] = 'exclam'
            elif final_keys[i] == ':':
                final_keys[i] = 'colon'
            elif final_keys[i] == '-':
                final_keys[i] = 'minus'
            elif final_keys[i] == ' ':
                final_keys[i] = 'space'
            elif final_keys[i] == ')':
                final_keys[i] = 'parenright'
            elif final_keys[i] == '_' and step.get('layout', 'azerty') == 'qwerty':
                final_keys[i] = 'shift+parenright'
            elif final_keys[i] == '~' and step.get('layout', 'azerty') == 'qwerty':
                final_keys[i] = 'shift+twosuperior'
            elif final_keys[i].isupper():
                final_keys[i] = 'shift+' + final_keys[i]

        for key in final_keys:
            # Special case to allow for waiting (e.g. between a
            # username and a password, at the console login prompt):
            sleeper = re.match(r'^sleep:([0-9.]+)', key)
            if sleeper:
                print('[sleep:%s]' % sleeper.group(1), end=' ')
                time.sleep(float(sleeper.group(1)))
                continue

            # Normal case:
            self.send_key(key)
            print(key, end=' ')
            time.sleep(0.01)
        print()

    def take_screenshot(self, retry=0):
        screenshot_file = ki.get_screenshot(self.winid, self.snapdir, retry=retry)
        new_image = PIL.Image.open(screenshot_file)
        if self.top_margin > 0:
            new_image = ki.crop_image(new_image, self.top_margin)
            new_file = screenshot_file.replace('.png', '-cropped.png')
            new_image.save(new_file)
            return new_file
        else:
            return screenshot_file


if __name__ == '__main__':
    # Check current desktop and its windows:
    WINDOWS = get_windows(get_current_desktop())
    print("Current desktop:", get_current_desktop())
    print("Windows on current desktop:", WINDOWS)

    AUTOMAT = KaditAutomat()

    # FIXME: might be helpful to give some info about the automat.

    # Additionally, given a "reset" param, move mouse around:
    if len(sys.argv) > 1 and sys.argv[1] == 'reset':
        print("Resetting mouse location")
        AUTOMAT.reset_mouse_to_center()
