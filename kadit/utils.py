#!/usr/bin/python3
# coding: utf8
# © 2017-2019 Cyril Brulebois <kibi@debian.org>

import os.path
import re
import time

from colorama import init, Fore, Style
init(autoreset=True)


def compute_specific_delay(max_delay):
    if max_delay <= 10:
        # up to half a second:
        return max_delay / 20.0
    elif max_delay <= 60:
        # up to 2 seconds:
        return max_delay / 30.0
    else:
        # no more than 5 seconds
        return 5


def wait_specific_delay(max_delay):
    time.sleep(compute_specific_delay(max_delay))


def report_run_time(start):
    end = time.time()
    print(Fore.BLUE + Style.DIM + "RUNTIME   ", end=' ')
    print("%.1f seconds" % (end-start))


def get_playbook_basedir(playbook_file):
    basedir = re.sub(r'\.yml$', r'', playbook_file)
    # FIXME: Maybe check the directory's existence? But one could have
    # a playbook without extra files, so only make this a warning?
    return basedir


def find_image_file(image_file, search_paths, is_mandatory=True):
    found_image_path = None
    found_mask_path = []

    # First things first: image lookup
    for path in search_paths:
        image_path = os.path.join(path, image_file)
        if os.path.isfile(image_path):
            found_image_path = image_path
            break

    # Check whether mask + white files exist:
    if found_image_path:
        mask_path = re.sub(r'(.+)([.][^.]+)', r'\1_mask\2', found_image_path)
        white_path = re.sub(r'(.+)([.][^.]+)', r'\1_white\2', found_image_path)
        if os.path.isfile(mask_path) and os.path.isfile(white_path):
            found_mask_path = [mask_path, white_path]

    # Exception only if finding the image is mandatory:
    if found_image_path is None and is_mandatory:
        raise SystemError('Image file %s not found in search paths: %s' \
                          % (image_file, ':'.join(search_paths)))

    return found_image_path, found_mask_path
