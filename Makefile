#!/usr/bin/make -f

all: tests

tests:
	# Exclude kadit-record for now:
	pep8 -v $$(find -name 'kadit-run' -o -name '*.py')

.PHONY: all tests
