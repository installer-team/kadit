#!/usr/bin/python3
# coding: utf8
# © 2017 Cyril Brulebois <kibi@debian.org>

import PIL.Image
import PIL.ImageChops
import sys


def same_image(im1, im2):
    if im1.mode != im2.mode:
        print("Image mode mismatch: %s vs. %s" % (im1.mode, im2.mode))
        return False
    elif im1.size != im2.size:
        print("Size mismatch: %s vs. %s" % (im1.size, im2.size))
        return False
    else:
        return PIL.ImageChops.difference(im1, im2).getbbox() is None


def diff_image(im1, im2):
    return PIL.ImageChops.difference(im1, im2)

assert len(sys.argv) == 3

im1 = PIL.Image.open(sys.argv[1])
im2 = PIL.Image.open(sys.argv[2])
if same_image(im1, im2):
    print("Match")
    sys.exit(0)
else:
    diff = diff_image(im1, im2)
    print(diff)
    diff.show()
    print(diff.getbbox())
    (x1, y1, x2, y2) = diff.getbbox()
    print("dx:", (x2-x1))
    print("dy:", (y2-y1))
    area = (x2-x1) * (y2-y1)
    print("area:", area)
    diff.save("diff.png")
